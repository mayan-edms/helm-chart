# mayanedms

![Version: 4.5-1](https://img.shields.io/badge/Version-4.5--1-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: s4.5](https://img.shields.io/badge/AppVersion-s4.5-informational?style=flat-square)

Official Helm chart for Mayan EDMS

**Homepage:** <https://www.mayan-edms.com/>

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Roberto Rosario | <roberto.rosario@mayan-edms.com> |  |

## Source Code

* <https://gitlab.com/mayan-edms/mayan-edms>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for all of Mayan EDMS deployments. |
| beat.affinity | object | `{}` | Affinity for the specific deployments. |
| beat.resources.limits.cpu | string | `"1000m"` |  |
| beat.resources.limits.memory | string | `"1000Mi"` |  |
| beat.resources.requests.cpu | string | `"300m"` |  |
| beat.resources.requests.memory | string | `"384Mi"` |  |
| configuration.MAYAN_LOCK_MANAGER_BACKEND | string | `"mayan.apps.lock_manager.backends.redis_lock.RedisLock"` |  |
| frontend.affinity | object | `{"podAntiAffinity":{"preferredDuringSchedulingIgnoredDuringExecution":[{"podAffinityTerm":{"labelSelector":{"matchExpressions":[{"key":"app.kubernetes.io/component","operator":"In","values":["frontend"]}]},"topologyKey":"kubernetes.io/hostname"},"weight":100}]}}` | Frontend specific affinity. |
| frontend.autoscaling.enabled | bool | `false` |  |
| frontend.replicaCount | int | `2` |  |
| frontend.resources.limits.cpu | string | `"3000m"` |  |
| frontend.resources.limits.memory | string | `"3000Mi"` |  |
| frontend.resources.requests.cpu | string | `"200m"` |  |
| frontend.resources.requests.memory | string | `"512Mi"` |  |
| frontend.securityContext | object | `{}` |  |
| frontend.service.port | int | `8000` |  |
| frontend.service.type | string | `"ClusterIP"` |  |
| global | string | `nil` |  |
| image | object | `{"pullPolicy":"IfNotPresent","pullSecrets":[],"repository":"mayanedms/mayanedms"}` | Stack wide pullPolicy imageRegistry: "" namespace: mayan-edms storageClass: "" |
| ingress.annotations | object | `{}` |  |
| ingress.enabled | bool | `false` |  |
| ingress.hosts[0].host | string | `"mayan.home.arpa"` |  |
| ingress.hosts[0].paths[0] | string | `"/"` |  |
| ingress.tls | list | `[]` |  |
| livenessProbe.failureThreshold | int | `3` |  |
| livenessProbe.initialDelaySeconds | int | `5` |  |
| livenessProbe.periodSeconds | int | `10` |  |
| livenessProbe.successThreshold | int | `1` |  |
| livenessProbe.timeoutSeconds | int | `5` |  |
| persistence.core.annotations."helm.sh/resource-policy" | string | `"keep"` |  |
| persistence.core.enabled | bool | `true` |  |
| persistence.core.size | string | `"1Gi"` |  |
| persistence.core.volume.create | bool | `false` |  |
| readinessProbe.failureThreshold | int | `3` |  |
| readinessProbe.initialDelaySeconds | int | `5` |  |
| readinessProbe.periodSeconds | int | `10` |  |
| readinessProbe.successThreshold | int | `1` |  |
| readinessProbe.timeoutSeconds | int | `5` |  |
| secrets | object | `{}` |  |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.create | bool | `false` |  |
| serviceAccount.name | string | `nil` |  |
| startupProbe.failureThreshold | int | `27` |  |
| startupProbe.initialDelaySeconds | int | `30` |  |
| startupProbe.periodSeconds | int | `10` |  |
| startupProbe.successThreshold | int | `1` |  |
| startupProbe.timeoutSeconds | int | `5` |  |
| workerDefaults.affinity | object | `{"podAntiAffinity":{"preferredDuringSchedulingIgnoredDuringExecution":[{"podAffinityTerm":{"labelSelector":{"matchExpressions":[{"key":"app.kubernetes.io/component","operator":"In","values":["worker"]}]},"topologyKey":"kubernetes.io/hostname"},"weight":100}]}}` | Affinity for all worker deployments. |
| workers.worker-a.autoscaling | object | `{"enabled":false}` | Worker specific affinity. affinity: {} |
| workers.worker-a.internalName | string | `"worker_a"` | Worker specific environment variables. environment: {} |
| workers.worker-a.nameSuffix | string | `"worker-a"` |  |
| workers.worker-a.replicaCount | int | `2` |  |
| workers.worker-a.resources.limits.cpu | string | `"3000m"` |  |
| workers.worker-a.resources.limits.memory | string | `"3000Mi"` |  |
| workers.worker-a.resources.requests.cpu | string | `"1000m"` |  |
| workers.worker-a.resources.requests.memory | string | `"1000Mi"` |  |
| workers.worker-a.securityContext | object | `{}` |  |
| workers.worker-b.autoscaling.enabled | bool | `false` |  |
| workers.worker-b.extraArguments | list | `[]` |  |
| workers.worker-b.internalName | string | `"worker_b"` |  |
| workers.worker-b.nameSuffix | string | `"worker-b"` |  |
| workers.worker-b.replicaCount | int | `1` |  |
| workers.worker-b.resources.limits.cpu | string | `"3000m"` |  |
| workers.worker-b.resources.limits.memory | string | `"3000Mi"` |  |
| workers.worker-b.resources.requests.cpu | string | `"1000m"` |  |
| workers.worker-b.resources.requests.memory | string | `"1000Mi"` |  |
| workers.worker-b.securityContext | object | `{}` |  |
| workers.worker-c.autoscaling.enabled | bool | `false` |  |
| workers.worker-c.extraArguments | list | `[]` |  |
| workers.worker-c.internalName | string | `"worker_c"` |  |
| workers.worker-c.nameSuffix | string | `"worker-c"` |  |
| workers.worker-c.replicaCount | int | `1` |  |
| workers.worker-c.resources.limits.cpu | string | `"3000m"` |  |
| workers.worker-c.resources.limits.memory | string | `"3000Mi"` |  |
| workers.worker-c.resources.requests.cpu | string | `"1000m"` |  |
| workers.worker-c.resources.requests.memory | string | `"1000Mi"` |  |
| workers.worker-c.securityContext | object | `{}` |  |
| workers.worker-d.autoscaling.enabled | bool | `false` |  |
| workers.worker-d.extraArguments | list | `[]` |  |
| workers.worker-d.internalName | string | `"worker_d"` |  |
| workers.worker-d.nameSuffix | string | `"worker-d"` |  |
| workers.worker-d.replicaCount | int | `1` |  |
| workers.worker-d.resources.limits.cpu | string | `"4000m"` |  |
| workers.worker-d.resources.limits.memory | string | `"4000Mi"` |  |
| workers.worker-d.resources.requests.cpu | string | `"1000m"` |  |
| workers.worker-d.resources.requests.memory | string | `"1000Mi"` |  |
| workers.worker-d.securityContext | object | `{}` |  |
| workers.worker-e.autoscaling.enabled | bool | `false` |  |
| workers.worker-e.extraArguments | list | `[]` |  |
| workers.worker-e.internalName | string | `"worker_e"` |  |
| workers.worker-e.nameSuffix | string | `"worker-e"` |  |
| workers.worker-e.replicaCount | int | `1` |  |
| workers.worker-e.resources.limits.cpu | string | `"3000m"` |  |
| workers.worker-e.resources.limits.memory | string | `"3000Mi"` |  |
| workers.worker-e.resources.requests.cpu | string | `"1000m"` |  |
| workers.worker-e.resources.requests.memory | string | `"1000Mi"` |  |
| workers.worker-e.securityContext | object | `{}` |  |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.11.2](https://github.com/norwoodj/helm-docs/releases/v1.11.2)
