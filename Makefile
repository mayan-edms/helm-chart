#!make
include config.env

ifneq ($(wildcard config-user.env),)
	include config-user.env
endif

help:
	@echo "Usage: make <target>\n"
	@awk 'BEGIN {FS = ":.*##"} /^[0-9a-zA-Z_-]+:.*?## / { printf "  * %-40s -%s\n", $$1, $$2 }' $(MAKEFILE_LIST)|sort

gitlab-ci-publish: ## Build and publish the helm repository.
gitlab-ci-publish:
	git push ${GIT_REMOTE_NAME} :publish || true
	git push ${GIT_REMOTE_NAME} HEAD:publish

helm-package: ## Create a Helm repository package.
helm-package:
	wget charts.mayan-edms.com/index.yaml --output-document=build/index.yaml || true
	helm package mayan-edms --destination ./build
	helm repo index --merge build/index.yaml build/ --url ${HELM_REPO_URL}
