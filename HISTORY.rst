4.5-2 (2023-10-19)
==================
- Merge existing index.yaml files.

4.5-1 (2023-10-09)
==================
- Initial release.
- Support Mayan EDMS series 4.5.
- Support new Mayan EDMS worker E.
